var schools = [{
        name: 'ECSU',
        id: 'ecsu_modal'
    },
    {
        name: 'ECU',
        id: 'ecu_modal'
    },
    {
        name: 'UNCCH',
        id: 'uncch_modal'
    },
    {
        name: 'ODU',
        id: 'odu_modal'
    },
    {
        name: 'Millersville',
        id: 'millersville_modal'
    },
    {
        name: 'UGA',
        id: 'uga_modal'
    },
    {
        name: 'Marymount',
        id: 'marymount_modal'
    },
    {
        name: 'Duke',
        id: 'duke_modal'
    },
    {
        name: 'Troy',
        id: 'troy_modal'
    },
];
var getHtmlAndAppendToPageAndThenInitModal = function (id) {
    $('#schoolModalContainer').load("schoolModals/" + id + ".html", function () {
        console.log("Load was performed.");
        $('#' + id).modal()
        $('#' + id).modal('open');
    });

}
var initSchools = function () {
    for (const school in schools) {
        var name = schools[school].name;
        var id = schools[school].id;
        var li = $('<li></li>');
        var a = $('<a class="waves-effect" href="#!"></a>');
        a.html(name);
        var setClickEvent = (function (id) {
            a.click(function () {
                console.log(id)
                getHtmlAndAppendToPageAndThenInitModal(id);

            });
        })(id);

        li.append(a);
        $('.schoolsOfPsychologyList').append(li);
    }
}
