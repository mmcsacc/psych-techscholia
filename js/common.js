var transitionToPage = function (pageId) {
    $.mobile.changePage('#' + pageId, {
        transition: "fade",
        reverse: false,
        changeHash: true
    });
}
console.log("window NOT ready");
var onHoverListener = function (jqueryId, src1, src2) {
    $(jqueryId).hover(
        function () {
            console.log($(this).attr('src', src1));
        },
        function () {
            console.log($(this).attr('src', src2))
        }
    );
}
$(function () {
    document.onkeydown = checkKey;

    function checkKey(e) {

        e = e || window.event;

        if (e.keyCode == '38') {
            // up arrow
        } else if (e.keyCode == '40') {
            // down arrow
        } else if (e.keyCode == '37') {
            // left arrow
            $('.navPrevious')[0].click();
        } else if (e.keyCode == '39') {
            // right arrow
            $('.navNext')[0].click()
        }

    }
    console.log("window ready", window.location.hash);
    if (window.location.hash == "") {
        $('#preface').load("pages/preface.html", function () {
            console.log("Load was performed.");
            $('.materialboxed').materialbox();
        });
    };

})
jQuery(window).load(function () {
    console.log('widnow has loaded');
    // loading_screen.finish();
    $('#initLoader').remove();
});
var sideNavList = [{
        id: 'preface',
        name: 'Preface'
    },
    {
        id: 'someHistoryOfPsychology_1',
        name: 'Some History of Psychology'
    },
    {
        id: 'theScienceOfPsychology_1',
        name: 'The Science of Psychology'
    },
    {
        id: 'biopsychology_1',
        name: 'Biopsychology'
    },
    {
        id: 'conciousnessStatesOfMind_1',
        name: 'Conciousness/ States of Mind'
    },
    {
        id: 'developmentalPsychAndLanguage_1',
        name: 'Developmental Psych and Language'
    },
    {
        id: 'sensationAndPerception_1',
        name: 'Sensation and Perception'
    },
    {
        id: 'learning_1',
        name: 'Learning'
    },
    {
        id: 'memory_1',
        name: 'Memory'
    },
    {
        id: 'thinkingAndIntelligence_1',
        name: 'Thinking and Intelligence'
    },
    {
        id: 'motivationAndEmotion_1',
        name: 'Motivation and Emotion'
    },
    {
        id: 'ui-disabled',
        name: 'Stress, Health and SWB'
    },
    {
        id: 'personality_1',
        name: 'Personality'
    },
    {
        id: 'psychopathology_1',
        name: 'Psychopathology'
    },
    {
        id: 'ui-disabled',
        name: 'Psycotherapy'
    },
    {
        id: 'socialPsychology_1',
        name: 'Social Psychology'
    }
]
var getPagesFromGroup = {
    someHistoryOfPsychology: ['someHistoryOfPsychology_1', 'someHistoryOfPsychology_2', 'someHistoryOfPsychology_3', 'someHistoryOfPsychology_4'],
    theScienceOfPsychology: ['theScienceOfPsychology_1', 'theScienceOfPsychology_2', 'theScienceOfPsychology_3', 'theScienceOfPsychology_4', 'theScienceOfPsychology_5'],
    biopsychology: ['biopsychology_1', 'biopsychology_2', 'biopsychology_3', 'biopsychology_4', 'biopsychology_5', 'biopsychology_6'],
    conciousnessStatesOfMind: ['conciousnessStatesOfMind_1', 'conciousnessStatesOfMind_2', 'conciousnessStatesOfMind_3', 'conciousnessStatesOfMind_4', 'conciousnessStatesOfMind_5'],
    developmentalPsychAndLanguage: ['developmentalPsychAndLanguage_1', 'developmentalPsychAndLanguage_2', 'developmentalPsychAndLanguage_3', 'developmentalPsychAndLanguage_4', 'developmentalPsychAndLanguage_5', 'developmentalPsychAndLanguage_6', 'developmentalPsychAndLanguage_7', 'developmentalPsychAndLanguage_8'],
    sensationAndPerception: ['sensationAndPerception_1', 'sensationAndPerception_2', 'sensationAndPerception_3', 'sensationAndPerception_4', 'sensationAndPerception_5'],
    learning: ['learning_1', 'learning_2', 'learning_3', 'learning_4', 'learning_5'],
    memory: ['memory_1', 'memory_2', 'memory_3', 'memory_4', 'memory_5'],
    thinkingAndIntelligence: ['thinkingAndIntelligence_1', 'thinkingAndIntelligence_2', 'thinkingAndIntelligence_3', 'thinkingAndIntelligence_4', 'thinkingAndIntelligence_5'],
    motivationAndEmotion: ['motivationAndEmotion_1', 'motivationAndEmotion_2', 'motivationAndEmotion_3', 'motivationAndEmotion_4', 'motivationAndEmotion_5', 'motivationAndEmotion_6', 'motivationAndEmotion_7'],
    personality: ['personality_1', 'personality_2', 'personality_3', 'personality_4'],
    psychopathology: ['psychopathology_1', 'psychopathology_2', 'psychopathology_3', 'psychopathology_4', 'psychopathology_5'],
    socialPsychology: ['socialPsychology_1', 'socialPsychology_2', 'socialPsychology_3', 'socialPsychology_4']
}


$(function () {
    for (var i = 0; i < sideNavList.length; i++) {
        var li = $('<li></li>');
        var classStr = sideNavList[i].id.replace('_1', '');
        li.attr('class', classStr);

        var link = $('<a class="waves-effect" href="#!"></a>');
        setClickEvent = function (sideNavList, link, i) {
            link.click(function () {
                transitionToPage(sideNavList[i].id);
                // $('#sideNav').sideNav('hide');
            });
        }(sideNavList, link, i)

        link.html(sideNavList[i].name);
        li.append(link);
        $('#sideNavMainPageList').append(li);
    }
    for (var page in pages) {
        var pageObj = pages[page];
        var prevPageNum = parseInt(page) - 1;
        var nextPageNum = parseInt(page) + 1;
        var previousPageObj = pages[prevPageNum.toString()];
        var nextPageObj = pages[nextPageNum.toString()];
        var run = function (pageObj, previousPageObj, nextPageObj, pageNum) {
            jQuery("#" + pageObj.id).on("pagebeforeshow", function (event) {
                guessChoices = pageObj.guessChoices;
                hints = pageObj.hints;

                xcards = 0;
                cfarray = pageObj.cfarray;
                arraylength = cfarray.length - 1;
                cbarray = pageObj.cbarray;
                document.getElementById("cf").innerHTML = cfarray[xcards];
                document.getElementById("cb").innerHTML = cbarray[xcards];
                if (typeof pageObj.crosswordLink !== 'undefined') {
                    $('.crosswordLink').attr('href', pageObj.crosswordLink);
                } else {
                    // this is temporary and should remain here until all pages have a crosswordLink
                    $('.crosswordLink').attr('href', 'http://psych.techscholia.com/book/puzzles/ch1/Zimbardo_HistoryScience.html');
                }
                if (typeof pageObj.references !== 'undefined') {
                    $('.referenceContainer').css('display', 'block');
                    $('#appendReferences').empty();
                    $('.removeForRef').css('display', 'none');
                    for (let i = 0; i < pageObj.references.length; i++) {

                        var reference = pageObj.references[i];
                        $('#appendReferences').append($(reference));
                        $('#appendReferences').append($('<br>'));
                    }
                } else {
                    $('.referenceContainer').css('display', 'none');
                    $('.removeForRef').css('display', 'block');
                }

                startAgain();
                if (pageObj.isPartOfGroup) {

                    $('#' + pageObj.id).load("pages/" + pageObj.groupName + "/" + pageObj.id + ".html", function () {
                        console.log("Load was performed.");
                        $('.materialboxed').materialbox();
                        if (pageObj.id == 'biopsychology_1') {
                            onHoverListener('#onHoverDnaPicture', 'img/DNA_80.png', 'img/DNA_icon.png');
                        } else if (pageObj.id == 'biopsychology_3') {
                            onHoverListener('#onHoverSleep', 'img/Transmitters.jpg', 'img/Transmitters_2.jpg');
                        } else if (pageObj.id == 'biopsychology_4') {
                            onHoverListener('#onHoverDopamine', 'img/Dopamine.jpg', 'img/Dopamine_2.jpg');
                        } else if (pageObj.id == 'conciousnessStatesOfMind_3') {
                            onHoverListener('#onHoverSleepPicture', 'img/Sleep_Hours.png', 'img/Sleep_Hours_icon.png');
                        } else if (pageObj.id == 'conciousnessStatesOfMind_2') {
                            $('select').material_select();
                        } else if (pageObj.id == 'sensationAndPerception_3') {
                            $('.collapsible').collapsible();
                            $('#hoverYoungLady').hover(
                                function () {
                                    $('#hoverYoungLady').collapsible('open', 0);
                                },
                                function () {
                                    $('#hoverYoungLady').collapsible('close', 0);
                                }
                            );
                            $('#hoverOldLady').hover(
                                function () {
                                    $('#hoverOldLady').collapsible('open', 0);
                                },
                                function () {
                                    $('#hoverOldLady').collapsible('close', 0);
                                }
                            );
                            onHoverListener('#onHoverStarOfDavid', 'img/Sensation_closure_SOD.png', 'img/Sensation_closure_triangle.png');

                        } else if (pageObj.id == 'thinkingAndIntelligence_2') {
                            $('.collapsible').collapsible();
                            $('#intelligenceTriangleCircleCard').hover(
                                function () {
                                    $('#intelligenceTriangleCircleCard').collapsible('open', 0);
                                },
                                function () {
                                    $('#intelligenceTriangleCircleCard').collapsible('close', 0);
                                }
                            );
                            $('#intelligenceMazeCard').hover(
                                function () {
                                    $('#intelligenceMazeCard').collapsible('open', 0);
                                },
                                function () {
                                    $('#intelligenceMazeCard').collapsible('close', 0);
                                }
                            );
                            onHoverListener('#pictureOf9Dots', 'img/9dot-solution.png', 'img/9dot-img.png');

                        } else if (pageObj.id == "socialPsychology_2") {
                            // put the answer in the second parameter
                            onHoverListener('#aschLines', /*answer goes here --> */ 'img/Asch_lines.png' /* <-- answer goes here*/ , 'img/Asch_lines.png');
                        }

                    });
                } else {
                    $('#' + pageObj.id).load("pages/" + pageObj.id + ".html", function () {
                        console.log("Load was performed.");
                        $('.materialboxed').materialbox();
                        if (pageObj.id == 'jobsForPsychologyMajors' || pageObj.id == 'schoolsOfPsychology') {
                            $('#jobsForPsychologyMajorsMenuTrigger').sideNav({closeOnClick: true});
                            initSchools();
                        }
                    });
                }
                // $( '#'+pageObj.id ).load( "pages/"+ pageObj.id+".html", function() {
                //     console.log( "Load was performed." );
                // });
                // console.log('thisPage',pageObj,'nextPage',nextPageObj,'prevPage',previousPageObj)
                $('#sideNav li.active').removeClass('active')
                $($('#sideNav li.' + pageObj.groupName)).addClass('active')


                $("title").html(pageObj.name);

                if (pageObj.isPartOfGroup) {
                    $('.audioOnBottom paper-audio-player').attr('src', 'audio/' + pageObj.groupName + '/' + pageObj.id + '.mp3');
                } else {
                    $('.audioOnBottom paper-audio-player').attr('src', 'audio/' + pageObj.id + '.mp3');
                }


                if (pageObj.id == 'jobsForPsychologyMajors' || pageObj.id == 'schoolsOfPsychology') {
                    $('.audioOnBottom paper-audio-player').addClass('hide');
                    if (pageObj.id == 'jobsForPsychologyMajors') {
                        $('.addMarginForSecondMenu').addClass('marginForSecondMenu');
                    }
                } else {
                    $('.audioOnBottom paper-audio-player').removeClass('hide');
                }
                if (pageObj.id !== 'jobsForPsychologyMajors') {
                    $('.addMarginForSecondMenu').removeClass('marginForSecondMenu');
                }
                // if (pageObj.id == 'jobsForPsychologyMajors' || pageObj.id == 'schoolsOfPsychology'){
                //     $('.audioOnBottom paper-audio-player').addClass('hide');
                //     $('.addMarginForSecondMenu').addClass('marginForSecondMenu');
                // } else {
                //     $('.audioOnBottom paper-audio-player').removeClass('hide');
                //     $('.addMarginForSecondMenu').removeClass('marginForSecondMenu');
                // }

                $('.audioOnBottom paper-audio-player').attr('title', pageObj.name);

                // $('paper-audio-player[color="orange"] #progress').css('background-color','#29b6f6');
                // $('paper-audio-player[color="#29b6f6"] #progress').css('background-color','#ff9800');

                $('#logo-container').html(pageObj.name);
                if (pageObj.isPartOfGroup) {
                    $('#paginationPages').empty();
                    for (var i = 0; i < pageObj.numOfPages; i++) {
                        var li = $('<li class="waves-effect"></li>');
                        var a = $('<a href="#!"></a>');
                        a.html(i + 1);
                        var setClickEvent = function (index) {
                            a.click(function (elm) {
                                transitionToPage(getPagesFromGroup[pageObj.groupName][index])
                            })
                        }(i)

                        li.append(a);
                        $('#paginationPages').append(li);
                    }
                    $('.paginationContainer').css('display', 'block');
                    $('.navButtonsContainer').css('display', 'none');
                    for (var i = 0; i < $('#paginationPages li').length; i++) {
                        $($('#paginationPages li')[i]).removeClass('active');
                    }
                    $($('#paginationPages li')[pageObj.index]).addClass('active');
                } else {
                    $('.paginationContainer').css('display', 'none');
                    $('.navButtonsContainer').css('display', 'block');
                }
                if (previousPageObj) {
                    $('.navPrevious').attr('onclick', 'transitionToPage(\'' + previousPageObj.id + '\')');
                    $('.navPrevious').css('cursor', 'pointer');
                    $('.navPrevious').removeClass('grey-text text-darken-2');
                    $('.navPrevious').removeClass('hide');

                } else {
                    $('.navPrevious').css('cursor', 'not-allowed');
                    $('.navPrevious').addClass('grey-text text-darken-2');
                    $('.navPrevious').addClass('hide');
                }
                if (nextPageObj) {
                    $('.navNext').attr('onclick', 'transitionToPage(\'' + nextPageObj.id + '\')');
                    $('.navNext').css('cursor', 'pointer');
                    $('.navNext').removeClass('grey-text text-darken-2');
                    $('.navNext').removeClass('hide');
                } else {
                    $('.navNext').css('cursor', 'not-allowed')
                    $('.navNext').addClass('grey-text text-darken-2')
                    $('.navNext').addClass('hide');
                }
            });
        }(pageObj, previousPageObj, nextPageObj, page)




    };
});

var regularParagraph = function (text, onlyOneBr, noBr) {
    var p = $('<p class="flow-text"></p>');
    p.html(text);
    return p[0].outerHTML;
}

var paragraphWithPhoto = function (text, shouldBeOnRight, dataCaption, src) {
    var colS6M4 = $('<div class="col s6 m4"></div>');
    if (shouldBeOnRight) {
        colS6M4.addClass('right');
    };
    var img = $('<img class="materialboxed">')
    img.attr('data-caption', dataCaption);
    img.attr('src', src);
    colS6M4.append(img);
    var p = $('<p class="flow-text"></p>');
    p.html(text);
    return colS6M4[0].outerHTML + p[0].outerHTML;
}

var videoTag = function (src) {
    var div = $('<div class="video-container"></div>');
    var iframe = $('<iframe width="853" height="480" frameborder="0" gesture="media" allowfullscreen></iframe>')
    iframe.attr('src', src);
    div.append(iframe);

    return div[0].outerHTML
}
