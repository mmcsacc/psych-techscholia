var xcards = 0;
var cfarray = new Array("This area is not yet populated","Why did you click that!?");
var arraylength = cfarray.length - 1
var cbarray = new Array("Will be occupied soon","Did you not beleive me??");

function nextCard() {
    xcards = xcards + 1
    if (xcards > arraylength) {
        xcards = 0
    }
    document.getElementById("cf").innerHTML = cfarray[xcards]
    document.getElementById("cb").innerHTML = cbarray[xcards]
}

function previousCard() {
    xcards = xcards - 1
    if (xcards < 0) {
        xcards = arraylength
    }
    document.getElementById("cf").innerHTML = cfarray[xcards]
    document.getElementById("cb").innerHTML = cbarray[xcards]
}



$('#flashCard')
.mouseover(function() {
  $('#flashCard').css('overflow', 'hidden');
  $('#flashCard').find('.card-reveal').css({ display: 'block' }).velocity("stop", false).velocity({ translateY: '-100%' }, { duration: 300, queue: false, easing: 'easeInOutQuad' });  
})
.mouseout(function() {
    $('#flashCard').find('.card-reveal').velocity({
        translateY: 0
    }, {
        duration: 225,
        queue: false,
        easing: 'easeInOutQuad',
        complete: function () {
            $(this).css({
                display: 'none'
            });
            $('#flashCard').css('overflow', $('#flashCard').data('initialOverflow'));
        }
    });
});