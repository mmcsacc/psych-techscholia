var x = 0;
var D = 0;
//<!-- Set up hangman array -->
gallows = new Array("--------\n|      |\n|\n|\n|\n|\n|\n=========",
    "--------\n|      |\n|      O\n|\n|\n|\n|\n=========",
    "--------\n|      |\n|      O\n|      |\n|\n|\n|\n=========",
    "--------\n|      |\n|      O\n|     \\|\n|\n|\n|\n=========",
    "--------\n|      |\n|      O\n|     \\|/\n|\n|\n|\n=========",
    "--------\n|      |\n|      O\n|     \\|/\n|      |\n|\n|\n=========",
    "--------\n|      |\n|      O\n|     \\|/\n|      |\n|     /\n|\n=========",
    "--------\n|      |\n|      @\n|     \\|/\n|      |\n|     / \\\n|\n=========")

function Next() {
    x = x + 1
    if (x > len) {
        x = 0
    }
    startAgain()
}



function Repeat() {
    startAgain()
}

function Previous() {
    x = x - 1
    if (x < 0) {
        x = len
    }
    startAgain()
}

function startAgain() {
    D = 0
    guesses = 0
    max = gallows.length - 1
    guessed = " "
    len = guessChoices.length - 1
    toGuess = guessChoices[x].toUpperCase()
    displayHangman()
    displayToGuess()
    words = hints[x]
    document.querySelector('#Hcontent').innerHTML = ''
    document.getElementById("A").style.backgroundColor = "azure"
    document.getElementById("B").style.backgroundColor = "azure"
    document.getElementById("C").style.backgroundColor = "azure"
    document.getElementById("D").style.backgroundColor = "azure"
    document.getElementById("E").style.backgroundColor = "azure"
    document.getElementById("F").style.backgroundColor = "azure"
    document.getElementById("G").style.backgroundColor = "azure"
    document.getElementById("H").style.backgroundColor = "azure"
    document.getElementById("I").style.backgroundColor = "azure"
    document.getElementById("J").style.backgroundColor = "azure"
    document.getElementById("K").style.backgroundColor = "azure"
    document.getElementById("L").style.backgroundColor = "azure"
    document.getElementById("M").style.backgroundColor = "azure"
    document.getElementById("N").style.backgroundColor = "azure"
    document.getElementById("O").style.backgroundColor = "azure"
    document.getElementById("P").style.backgroundColor = "azure"
    document.getElementById("Q").style.backgroundColor = "azure"
    document.getElementById("R").style.backgroundColor = "azure"
    document.getElementById("S").style.backgroundColor = "azure"
    document.getElementById("T").style.backgroundColor = "azure"
    document.getElementById("U").style.backgroundColor = "azure"
    document.getElementById("V").style.backgroundColor = "azure"
    document.getElementById("W").style.backgroundColor = "azure"
    document.getElementById("X").style.backgroundColor = "azure"
    document.getElementById("Y").style.backgroundColor = "azure"
    document.getElementById("Z").style.backgroundColor = "azure"
    document.querySelector('#Hcontent').innerHTML = words
}


function stayAway() {
    document.game.elements[3].focus()
    alert("Don't mess with this form element!")
}

function displayHangman() {
    document.game.status.value = gallows[guesses]
}

function displayToGuess() {
    pattern = ""
    for (i = 0; i < toGuess.length; ++i) {
        if (guessed.indexOf(toGuess.charAt(i)) != -1)
            pattern += (toGuess.charAt(i) + " ")
        else pattern += "_ "
    }
    document.game.toGuess.value = pattern
}


function badGuess(s) {
    if (toGuess.indexOf(s) == -1) return true
    return false
}

function winner() {
    for (i = 0; i < toGuess.length; ++i) {
        if (guessed.indexOf(toGuess.charAt(i)) == -1) return false
    }
    return true
}

function guess(s) {
    if ((document.getElementById(s).style.backgroundColor != "gray") && D === 0) {
        document.getElementById(s).style.backgroundColor = "gray"
        if (guessed.indexOf(s) == -1) guessed = s + guessed
        if (badGuess(s)) ++guesses
        displayHangman()
        displayToGuess()

        if (guesses >= max) {
            document.querySelector('#Hcontent').innerHTML = 'Too Many Guesses! - - If you want to play again,<br>Click the \"next\" \"repeat\" or \"previous\" button'
            D = 1
        }
    }
    if (winner()) {
        document.querySelector('#Hcontent').innerHTML = 'YOU WON!! - -If you want to play again,<br>Click the \"next\" \"repeat\" or \"previous\" button'
        D = 1
    }
}